---
title: Kotlin Skeleton
description: Rest API based in spring boot + postgreSQL
layout: base.njk
---

# Kotlin Skeleton
Rest API based in spring boot + postgreSQL

## local setup

**docker compose**

For local app postgreSQL and sonarqube: `docker-compose -f docker-compose-local.yml up -d`

Run docker compose: `docker-compose up -d`

**git prehook**

* pre-commit run linter `./gradlew ktlintCheck`
* pre-push run tests `./gradlew test integrationTest jacoco`

### linter

Using [Ktlint Gradle](https://github.com/JLLeitschuh/ktlint-gradle) as a linter tool

run local analysis: `./gradlew ktlintCheck`

Reports in build/reports/klint

### Test

Tools

* Integration Test with TestContainers: https://www.testcontainers.org/
* Kong Unirest: HTTP client library http://kong.github.io/unirest-java/
* MockK: mocking library http://mockk.io
* Test coverage: [jacoco](https://www.eclemma.org/jacoco/)

Commands

* run unit test: `./gradlew test` Report in build/reports/test
* run unit test: `./gradlew integrationTest` Report in build/reports/integrationTest

### build application

Build application: `gradle :build`

Build assemble: `gradle :assemble`

Run app: `gradle :bootRun`

### Analysis

run local analysis: `SONARQUBE_KEY=sqp_afe48c....a3 ./gradlew sonar`

config sonarlint https://ravitechverma.medium.com/minimize-technical-debts-by-integrating-sonarlint-with-sonarqube-da10ccea14ef

sonarqube in gitlab cicd https://medium.com/@alishayb/quality-assurance-using-sonarqube-gitlab-sonarqube-integration-f6ae61bc49f4

## CI CD

### gitlab image repository

login `docker login registry.gitlab.com`

push `docker push registry.gitlab.com/arturisimo/kotlin-skeleton`

### gitlab runner register

installation gitlab runner: https://docs.gitlab.com/runner/install/linux-repository.html

* check status `sudo gitlab-runner status`
* register `sudo gitlab-runner register`
* list configured runners `sudo gitlab-runner list`
* restart `sudo gitlab-runner restart`
* Docker-in-Docker requires privileged mode to function in `/etc/gitlab-runner/config.toml`

![gitlab runner](img/gitlab_runner.gif)

## Documentation

Using [lume](https://lume.land/) static site generator for [Deno](https://deno.com/). It generates html from markdown pages

[Gitlab pages](https://docs.gitlab.com/ee/user/project/pages/) allows to publish static websites from public folder using a job called "pages":

    deno task lume --dest=public

Documentation deployed in https://arturisimo.gitlab.io/kotlin-skeleton/

## Deployment

Deploy in [okteto](https://www.okteto.com/)

deploy dev environment `okteto up`

deploy `okteto deploy`

rebuilding images `okteto deploy --build`

destroy dev environment `okteto destroy`

Api deployed in https://kotlin-skeleton-arturisimo.cloud.okteto.net
