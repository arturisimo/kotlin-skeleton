import lume from "lume/mod.ts";

const site = lume();

site.copy("img");

export default site;
