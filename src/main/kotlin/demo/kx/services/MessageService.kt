package demo.kx.services

import demo.kx.dto.Message
import demo.kx.repository.MessageRepository
import org.springframework.stereotype.Service

@Service
class MessageService(val messageRepository: MessageRepository) {
    fun findMessages(): List<Message> = messageRepository.findAll()
    fun findMessageById(id: String): Message? = messageRepository.findById(id).orElse(null)
    fun post(message: Message) = messageRepository.save(message)
    fun deleteAll() = messageRepository.deleteAll()
}
