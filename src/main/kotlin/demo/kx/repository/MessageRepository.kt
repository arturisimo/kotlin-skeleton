package demo.kx.repository

import demo.kx.dto.Message
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository
@Repository
interface MessageRepository : MongoRepository<Message, String>
