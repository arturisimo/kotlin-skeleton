package demo

import demo.kx.dto.Message
import demo.kx.extensions.uuid
import demo.kx.repository.MessageRepository
import kong.unirest.HttpResponse
import kong.unirest.Unirest
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.server.LocalServerPort
import org.springframework.http.HttpStatus
import org.springframework.test.context.DynamicPropertyRegistry
import org.springframework.test.context.DynamicPropertySource
import org.testcontainers.containers.MongoDBContainer
import org.testcontainers.containers.Network
import org.testcontainers.containers.wait.strategy.Wait
import org.testcontainers.junit.jupiter.Container
import org.testcontainers.junit.jupiter.Testcontainers
import org.testcontainers.utility.DockerImageName
import java.time.Duration
import kotlin.random.Random

@SpringBootTest(
    webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
    classes = [DemoApplication::class]
)
@Testcontainers
class IntegrationTest(
    @Autowired val messageRepository: MessageRepository,
    @Autowired val mapper: com.fasterxml.jackson.databind.ObjectMapper
) {

    @LocalServerPort
    var port: Int? = null

    companion object {

        private val logger = LoggerFactory.getLogger(IntegrationTest::class.java)

        @JvmStatic
        @DynamicPropertySource
        fun datasourceConfig(registry: DynamicPropertyRegistry) {
            registry.add("spring.data.mongodb.database", { "messages" })
            registry.add("spring.data.mongodb.host", mongoDBContainer::getHost)
            registry.add("spring.data.mongodb.port", mongoDBContainer::getFirstMappedPort)
            val port: Int = mongoDBContainer.firstMappedPort
            val host: String = mongoDBContainer.replicaSetUrl
            logger.info("host mongodb: $host")
            logger.info("listening port of mongodb: $port")
            val containerIpAddress: String = mongoDBContainer.containerIpAddress
            val containerPort: Int = mongoDBContainer.getMappedPort(27017)
            logger.info("MongoDB Container IP: $containerIpAddress")
            logger.info("MongoDB Container Port: $containerPort")
            logger.info("MongoDB Connection string: ${mongoDBContainer.connectionString}")
            logger.info("MongoDB Container info ${mongoDBContainer.containerInfo}")
        }

        @Container
        val mongoDBContainer: MongoDBContainer = mongodb("mongo:6.0.6") {
            waitingFor(Wait.forLogMessage(".*Waiting for connections*\\s", 1))
            withNetwork(Network.newNetwork())
            // withLogConsumer(Slf4jLogConsumer(logger))
            withStartupAttempts(3)
            withStartupTimeout(Duration.ofSeconds(120))
            start()
        }
    }

    @AfterEach
    fun cleanup() {
        messageRepository.deleteAll()
    }

    @Test
    fun `mongodb container is up and running`() {
        Assertions.assertTrue(mongoDBContainer.isRunning)
    }

    @Test
    fun `test hello endpoint`() {
        val response: HttpResponse<String> = Unirest.get("http://localhost:$port/hello").asString()
        org.assertj.core.api.Assertions.assertThat(response.isSuccess)
        org.assertj.core.api.Assertions.assertThat(response.body).contains("Hello")
    }

    @Test
    fun `testing if we can post and retrieve the data`() {
        val id = "${Random.nextInt()}".uuid()
        val message = Message(id, "some message")

        Unirest.post("http://localhost:$port/api/messages")
            .header("Content-Type", "application/json")
            .header("Accept", "application/json")
            .body(mapper.writeValueAsString(message))
            .asString()

        val list: HttpResponse<String> = Unirest.get("http://localhost:$port/api/messages").asString()
        org.assertj.core.api.Assertions.assertThat(list.isSuccess)
        val messages = mapper.readValue(list.body, List::class.java)
        org.assertj.core.api.Assertions.assertThat(messages.size).isEqualTo(1)

        val response: HttpResponse<String> = Unirest.get("http://localhost:$port/api/messages/$id").asString()
        org.assertj.core.api.Assertions.assertThat(response.isSuccess)
        org.assertj.core.api.Assertions.assertThat(response.body).contains(message.id)
        org.assertj.core.api.Assertions.assertThat(response.body).contains(message.text)

        val msg = mapper.readValue(response.body, Message::class.java)
        org.assertj.core.api.Assertions.assertThat(msg.id).isEqualTo(message.id)
        org.assertj.core.api.Assertions.assertThat(msg.text).contains(message.text)
    }

    @Test
    fun `message not found`() {
        val id = "${Random.nextInt()}".uuid()
        val response: HttpResponse<String> = Unirest.get("http://localhost:$port/api/messages/$id").asString()
        org.assertj.core.api.Assertions.assertThat(response.status).isEqualTo(HttpStatus.NOT_FOUND.value())
    }
}

/**
 *  funcion de alto nivel para crear una instancia de los containers
 */
fun mongodb(imageName: String, opts: MongoDBContainer.() -> Unit) =
    MongoDBContainer(DockerImageName.parse(imageName)).apply(opts)
