FROM openjdk:17-jdk-slim
COPY --chown=nobody:nobody build/libs/kotlin-skeleton.jar app.jar
EXPOSE 8080
CMD [ "java", "-jar", "app.jar" ]

